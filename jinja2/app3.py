import ConfigParser

from flask import Flask, session

app = Flask(__name__)

def init(app):
  config = ConfigParser.ConfigParser()
  try:
    config_location = "etc/defaults.cfg"
    config.read(config_location)
    
    app.config['DEBUG'] = config.get("config", "debug")
    app.config['ip_address'] = config.get("config", "ip_address")
    app.config['port'] = config.get("config", "port")
    app.config['url'] = config.get("config", "url")
    app.config['secret_key'] = config.get("config", "secret_key")

    app.secret_key = app.config['secret_key']
  except:
    print "Config file couldn't be loaded"

@app.route('/')
def index():
  return "Root for the sessions example"
  
@app.route('/session/write/<name>')
def write(name=None):
  session['name'] = name
  return "Wrote %s into 'name' key of session" % name
  
@app.route('/session/read/')
def read():
  try:
    if(session['name']):
      return str(session['name'])
  except KeyError:
    pass
  return "No session variable there"
  
@app.route('/session/remove/')
def remove():
  session.pop('name', None)
  return "Removed key 'name' from session"
  
  
if __name__ == "__main__":
  init(app);
  app.run(
    host=app.config['ip_address'],
    debug=app.config['DEBUG'],
    port=int(app.config['port']))
