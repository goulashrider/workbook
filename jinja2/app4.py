import ConfigParser

from flask import Flask, session, flash, redirect, request, url_for, render_template

app = Flask(__name__)

def init(app):
  config = ConfigParser.ConfigParser()
  try:
    config_location = "etc/defaults.cfg"
    config.read(config_location)
    
    app.config['DEBUG'] = config.get("config", "debug")
    app.config['ip_address'] = config.get("config", "ip_address")
    app.config['port'] = config.get("config", "port")
    app.config['url'] = config.get("config", "url")
    app.config['secret_key'] = config.get("config", "secret_key")

    app.secret_key = app.config['secret_key']
  except:
    print "Config file couldn't be loaded"

@app.route('/')
def index():
  return render_template('index.html')

  return "Root Page"
   
@app.route('/login/<message>')
def login(message):
  if (message != None):
    flash(message)
  flash("Default message")
  return "Let's pretend you just logged in"

 
  
if __name__ == "__main__":
  init(app);
  app.run(
    host=app.config['ip_address'],
    debug=app.config['DEBUG'],
    port=int(app.config['port']))
